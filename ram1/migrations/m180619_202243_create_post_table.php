<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m180619_202243_create_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
              $this->createTable('post', [
           'id' => $this->primaryKey(),
           'title' => $this->string(), 
           'status' => $this->string(), 
          // 'descriptin' => $this->string(),
           'body' => $this->text(),
           'author_id' => $this->integer(),
       
           'category_id' => $this->integer(),
           'created_at' => $this->timestamp(),
           'updated_at' => $this->timestamp(),
           'created_by' => $this->integer(),
           'updated_by' => $this->integer(),
       ]);

    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post');
    }
    public function behaviors()
         {
             return [
             
                 [
                     'class' => BlameableBehavior::className(),
                     'createdByAttribute' => 'created_by',
                     'updatedByAttribute' => 'updated_by',
                 ],
                 'timestamp' => [
                     'class' => 'yii\behaviors\TimestampBehavior',
                     'attributes' => [
                         ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                         ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                     ],
                 ],
             ];
         }
}

